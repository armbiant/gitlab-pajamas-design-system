---
name: Text input
description: 
---

## Examples

<story-viewer story-name="base-form-form-input--default" title="Text input"></story-viewer>

## Structure

<todo>Add structure image.</todo>

## Guidelines

<todo>Add guidelines.</todo>

### Appearance

<todo>Add appearance.</todo>

### Behavior

<todo>Add behavior.</todo>

### Accessibility

<todo>Add accessibility.</todo>
